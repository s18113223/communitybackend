<?php

namespace App\Models\Entity;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostImageModel extends Model
{
    use HasFactory;
    protected $table = 'pageImage';
    // public $timestamps = true;
}
